# aws_ivs_player

Flutter Plugin for Aws Player Sdk.

## Getting Started

Import package
```
import 'package:aws_ivs_player/aws_ivs_player.dart';
```

Create Player Controller with stream url

```
controller = AwsIvsPlayerController("STREAM URL");
```

Init controller
```
controller.init();
```

Dispose controller
```
controller.dispose();
```

Listen for player state 
```
controller.getStateStream.listen((event) {
    ...
});
```
Wait for ready state befoew calling `play()`

Play stream
```
controller.play();
```

Get player state
```
controller.getState();
```

Get preview widget of player
```
controller.previewWidget();
```

Toggle video sound
```
controller.toggleMute()
```

