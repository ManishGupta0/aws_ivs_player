import 'dart:async';

import 'package:aws_ivs_player/src/channel_api.g.dart';
import 'package:flutter/material.dart';

class AwsIvsPlayerController extends IvsPlayerChannelCallback {
  AwsIvsPlayerController(this.url) : _api = IvsPlayerChannelApi();

  final IvsPlayerChannelApi _api;
  final String url;

  int _textureId = -1;

  final _stateController = StreamController<IvsPlayerState>.broadcast();

  Stream<IvsPlayerState> get getStateStream => _stateController.stream;

  @override
  void onStateChanged(IvsPlayerState state) {
    _stateController.add(state);
  }

  Future<void> init() async {
    await _api.init(url);
    _textureId = await _api.getTextureId();
  }

  Future<void> play() async {
    await _api.play();
  }

  Future<void> dispose() async {
    _api.dispose();
  }

  Future<void> toggleMute() async {
    _api.toggleMute();
  }

  Future<IvsPlayerState> getState() async {
    return await _api.getState();
  }

  Widget previewWidget() {
    return Texture(textureId: _textureId);
  }
}
