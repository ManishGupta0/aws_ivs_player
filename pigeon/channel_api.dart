import 'package:pigeon/pigeon.dart';

@ConfigurePigeon(
  PigeonOptions(
    dartOut: 'lib/src/channel_api.g.dart',
    javaOut:
        'android/src/main/java/com/aws_utils/ivs_player/aws_ivs_player/ChannelApi.java',
    javaOptions: JavaOptions(
      package: 'com.aws_utils.ivs_player.aws_ivs_player',
    ),
  ),
)
enum IvsPlayerState {
  buffering,
  playing,
  ready,
  idle,
  ended;
}

@HostApi()
abstract class IvsPlayerChannelApi {
  void init(String url);
  void dispose();
  int getTextureId();
  void play();
  void toggleMute();
  IvsPlayerState getState();
}

@FlutterApi()
abstract class IvsPlayerChannelCallback {
  void onStateChanged(IvsPlayerState state);
}
