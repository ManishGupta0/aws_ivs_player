package com.aws_utils.ivs_player.aws_ivs_player;

import android.content.Context;
import android.net.Uri;
import android.view.Surface;

import androidx.annotation.NonNull;

import com.amazonaws.ivs.player.Cue;
import com.amazonaws.ivs.player.Player;
import com.amazonaws.ivs.player.PlayerException;
import com.amazonaws.ivs.player.Quality;

import io.flutter.embedding.engine.plugins.FlutterPlugin;
import io.flutter.view.TextureRegistry;

public class ChannelApiImpl extends Player.Listener implements ChannelApi.IvsPlayerChannelApi {
    public TextureRegistry.SurfaceTextureEntry textureEntry;
    public Surface surface;
    public Player player;

    public ChannelApi.IvsPlayerChannelCallback callbacks;

    public ChannelApiImpl(FlutterPlugin.FlutterPluginBinding flutterPluginBinding){
        textureEntry = flutterPluginBinding.getTextureRegistry().createSurfaceTexture();
        surface = new Surface(textureEntry.surfaceTexture());

        player = Player.Factory.create(flutterPluginBinding.getApplicationContext());
        player.addListener(this);
        player.setSurface(surface);

        callbacks = new ChannelApi.IvsPlayerChannelCallback(flutterPluginBinding.getBinaryMessenger());
    }

    @Override
    public void init(String url) {
        player.load(Uri.parse(url));
    }

    @Override
    public void dispose() {
//        super.onDestroy();
        player.removeListener(this);
        player.release();
    }

    @NonNull
    @Override
    public Long getTextureId() {
        return textureEntry.id();
    }

    @Override
    public void play() {
        player.play();
    }

    @Override
    public void toggleMute(){
        player.setMuted(!player.isMuted());
    }

    @NonNull
    @Override
    public ChannelApi.IvsPlayerState getState() {
        return ChannelApi.IvsPlayerState.values()[player.getState().ordinal()];
    }


    @Override
    public void onCue(@NonNull Cue cue) {

    }

    @Override
    public void onDurationChanged(long l) {

    }

    @Override
    public void onStateChanged(@NonNull Player.State state) {
        callbacks.onStateChanged(ChannelApi.IvsPlayerState.values()[player.getState().ordinal()], new ChannelApi.Result<Void>() {
            @Override
            public void success(@NonNull Void result) {

            }

            @Override
            public void error(@NonNull Throwable error) {

            }
        });

        switch (state) {
            case BUFFERING:
                // player is buffering
                break;
            case READY:
                player.play();
                break;
            case IDLE:
                break;
            case PLAYING:
                // playback started
                break;
        }
    }

    @Override
    public void onError(@NonNull PlayerException e) {

    }

    @Override
    public void onRebuffering() {

    }

    @Override
    public void onSeekCompleted(long l) {

    }

    @Override
    public void onVideoSizeChanged(int i, int i1) {

    }

    @Override
    public void onQualityChanged(@NonNull Quality quality) {

    }

//    @Override
//    public void dispose() {
//        super.onDestroy();
//        player.removeListener(this);
//        player.release();
//    }
}
